@extends('app')
@section('page-heading')
City Weather Report
@endsection
@section('content')
<div class="row">
    <div class='col s12'>
        <a class="waves-effect waves-light btn"><i class="material-icons left">cloud</i>
            {{alertdata[0].description === "Sky is Clear" ? "Sky is Clear!No need to take an umbrella with you!" : "Sky is Cloudy!Please,take an umbrella with you!"}}
        </a>
    </div>
    <div class='col s3' ng-repeat="x in info">
        <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
                <img class="activator"    src="http://dreamatico.com/data_images/weather/weather-1.jpg">
            </div>
            <div class="card-content">
                <span class="card-title activator grey-text text-darken-4">{{x.name}}<i class="material-icons right">more_vert</i></span>
                <p>Clouds:{{x.weather[0].description}}&nbsp;&nbsp;Temperature:{{x.main.temp}}</p>
            </div>
            <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Full Reports<i class="material-icons right">close</i></span>
               
<hr>                <p>
                    Humidity:{{x.main.humidity}}<br>
                    Pressure:{{x.main.pressure}}<br>
                    Maximum Temperature:{{x.main.temp_max}}<br>
                    Minimum Temperature:{{x.main.temp_min}}<br>
                    Sunset:{{x.sys.sunset}}<br>
                    Sunrise:{{x.sys.sunrise}}<br>
                </p>
            </div>
        </div>
    </div>
 <!--<table class="bordered">
    <thead>
            <tr>
                <th data-field="id">City Name</th>
                <th data-field="name">Clouds</th>
                <th data-field="price">Temperature</th>
                <th data-field="price">Humidity</th>
                <th data-field="price">Wind Speed</th>
                <th data-field="price">Pressure</th>
            </tr>
    </thead>
  <tr ng-repeat="x in info">
    <td>{{x.name}}</td>
    <td>{{x.weather[0].description}}</td>
    <td>{{x.main.temp}}</td>
    <td>{{x.main.humidity}}</td>
    <td>{{x.wind.speed}}</td>
    <td>{{x.main.pressure}}</td>
  </tr>
</table>-->

</div>

   <script>
    var app = angular.module("myApp", []);
    app.controller("myCtrl", function($scope,$http) {
        $scope.info=[];
        $http.get("http://api.openweathermap.org/data/2.5/group?id=1336135,1273043,1185128,7671048,1336140,1861060,6251999,292223&units=metric&appid=44db6a862fba0b067b1930da0d769e98")
        .then(function (response) {$scope.info = response.data.list;});

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                mysrclat = position.coords.latitude; 
                mysrclong = position.coords.longitude;
                $scope.alertdata=[];
                alert_url="http://api.openweathermap.org/data/2.5/weather?lat="+mysrclat+"&lon="+mysrclong+"&appid=44db6a862fba0b067b1930da0d769e98"
               //alert(alert_url);
                $http.get(alert_url)
                .then(function (response) {
                    $scope.alertdata = response.data.weather;
                    if($scope.alertdata[0].description=="Sky is Clear"){
                        //alert("Sky is Clear!No need to take an umbrella with you!");
                    }
                    else{
                         //alert("Sky is Cloudy!Please,take an umbrella with you!");
                    }
                    //alert($scope.alertdata[0].description);

                });
            });
        
        }
    });
</script>
    @endsection
