@extends('app')

@section('content')

<div class="container" id="main_container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Test</div>
                <div class="panel-body">
                    <div ng-app="myApp" ng-controller="myCtrl">
                       [[$firstName]]
                    </div>
                    <script>
                        var app = angular.module("myApp", []);
                        app.controller("myCtrl", function($scope) {
                            $scope.firstName = "John";
                            $scope.lastName = "Doe";
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection